import React from 'react';
import { ScrollView, StyleSheet, Image, TouchableOpacity, View, Text } from 'react-native';
import MedDataListItem from '../components/MedDataListItem.js';

var medData = [
  {name: "ЭКГ", status: true, result: true},
  {name: "УЗИ сердца", status: true, result: false},
  {name: "Клинический анализ крови", status: true, result: false},
  {name: "Общий анализ мочи", status: true, result: true},
  {name: "ЛОР", status: false, result: true},
  {name: "Заключение врача-терапевта", status: false, result: true}
]

export default class MedDataScreen extends React.Component {
  static navigationOptions = {
      header: null
  }

  _renderMetDataList() {
      return medData.map((med, ind) => {
          return (
              <MedDataListItem name={med.name} status={med.status} result={med.result} key={ind} />
          )
      })
  }

  render() {
      return (
          <ScrollView style={styles.container}>
              <View style={{width: "100%", height: 66}}></View>
              <View style={styles.header}>
                  <View style={styles.logo}><Image  /></View>
                  <Text style={[styles.mainText, {marginBottom: 15}]}>Иванов Иван Иванович</Text>
                  <Text style={[styles.subText, {marginBottom: 30}]}>РИ-460013</Text>
                  <View style={styles.itemList}><Text style={styles.subText}>Группа крови</Text><Text style={styles.mainText}>IV положительная</Text></View>
                  <View style={styles.itemList}><Text style={styles.subText}>Адрес регистрации</Text><Text style={styles.mainText}>Коминтерна 5</Text></View>
                  <View style={styles.itemList}><Text style={styles.subText}>Номер полиса ОМС</Text><Text style={styles.mainText}>4545 6784 5412 1215</Text></View>
              </View>
              <View>
                  {this._renderMetDataList.bind(this)()}
              </View>
          </ScrollView>
      );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    paddingLeft: 40,
    paddingRight: 40,
    paddingBottom: 20,
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#F1F1F1",
    marginBottom: 20
  },
  logo: {
    width: 130,
    height: 130,
    borderRadius: 130,
    backgroundColor: "#F1F1F1",
    marginBottom: 16,
  },
  mainText: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: "sf-ui",
    color: "#5C5C5C"
  },
  subText: {
    fontSize: 12,
    lineHeight: 14,
    color: "#5C5C5C"
  },
  itemList: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    marginBottom: 20
  },
  indicator: {
    width: 25,
    height: 25,
    borderRadius: 25
  },
  medDataListItem: {
    flexDirection: "row",
    height: 25,
    marginBottom: 25,
    paddingRight: 40,
    paddingLeft: 40,
    justifyContent: "space-between",
    alignItems: "center"
  },
});
