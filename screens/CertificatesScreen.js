import React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import CertificateCard from '../components/CertificateCard';

var data = [
  {title: "Справка для занятия физической культурой", code: "У-046", progress: 80},
  {title: "Справка для ГТО", code: "Р-070", progress: 100},
  {title: "Справка для водительских прав", code: "Ф-032", progress: 50},
  {title: "Справка для общежития", code: "У-075", progress: 10},
  {title: "Справка для ГТО", code: "Р-070", progress: 100}
]


export default class CertificatesScreen extends React.Component {
    static navigationOptions = {
        header: null
    }

    _renderCards() {
        return data.map((cert, ind) => {
            return (
                <CertificateCard
                  key={ind} title={cert.title} code={cert.code}
                  progress={cert.progress}
                />
            )
        })
    }

    render() {
      return (
        <ScrollView style={styles.container}>
            <View style={{width: "100%", height: 66}}></View>
            {this._renderCards.bind(this)()}
        </ScrollView>
      );
    }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingRight: 16,
    paddingLeft: 16,
    backgroundColor: '#fff',
  },
});
