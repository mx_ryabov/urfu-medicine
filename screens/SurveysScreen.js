import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Calendar from '../components/Calendar.js';


export default class SurveysScreen extends React.Component {
    static navigationOptions = {
        header: null
    }

    openScheduleModal() {
      
    }

    render() {
        return (
            <View style={styles.container}>
                <Calendar updateTimeLine={() => {}} />
                <ScrollView>
                </ScrollView>
                <TouchableOpacity onPress={this.openScheduleModal.bind()} style={styles.floatBtn}>
                    <View style={{justifyContent: "center", alignItems: "center", position: "relative"}}>
                        <View style={[styles.line]}></View>
                        <View style={[styles.line, {transform: [{rotate: "90deg"}] } ]}></View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }

}


const styles = StyleSheet.create({
  line: {
    width: 3,
    height: 20,
    backgroundColor: "#fff",
    borderRadius: 5,
    position: "absolute",
    top: 17
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  floatBtn: {
    position: "absolute",
    width: 56,
    height: 56,
    borderRadius: 56,
    backgroundColor: "#32CF86",
    right: 32,
    bottom: 24
  }
});
