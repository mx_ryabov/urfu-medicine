/* @flow */

import React, { Component } from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity
} from 'react-native';

export default class Day extends Component {
  constructor(props) {
      super(props);
      this.state = {
          selected: false,
      }
  }

  select() {
      if (this.props.enabled) this.props.onPress();
  }

  _isAccess() {
      return this.props.enabled ? styles.enabledDay : styles.disabledDay;
  }

  render() {
      const styleDay = [this.props.selected && this.props.enabled ? styles.selected
        : this._isAccess.bind(this)(), styles.textDayStyle];

      return (
          <TouchableOpacity style={ [styles.dayStyle, styleDay] } onPress={this.select.bind(this)}>
              <Text style={ [styleDay, {borderWidth: 0}] }>{ this.props.date.getDate() }</Text>
          </TouchableOpacity>
      );
  }
}

const styles = StyleSheet.create({
  textDayStyle: {
    lineHeight: 22,
    textAlign: "center"
  },
  enabledDay: {
    color: "#A2A2A2",
  },
  disabledDay: {
    color: "rgba(200, 200, 200, .6)"
  },
  dayStyle: {
    width: 32,
    height: 32,
    alignItems: "center",
    justifyContent: "center",
  },
  selected: {
    color: "#32CF86",
    backgroundColor: "#fff",
    borderColor: "#32CF86",
    borderWidth: 1,
    borderRadius: 32
  }
});
