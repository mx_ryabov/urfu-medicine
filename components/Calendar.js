import React, { Component } from 'react';
import {
  View, Text, StyleSheet, Alert, TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';

import { data_calendar } from '../calendar.js';
import Day from './DayForCalendar.js';
import { actionCreators } from '../actions/allActions.js';



class Calendar extends Component {
  constructor(props) {
      super(props);
      let currentDate = new Date();
      this.weeks = data_calendar.getWeeks(currentDate.getFullYear(), currentDate.getMonth());

      this.state = {
          currentMonth: data_calendar.months[currentDate.getMonth()],
          currentNumberMonth: currentDate.getMonth(),
          currentYear: currentDate.getFullYear(),
          activeDay: new Date()
      }
  }

  _accessToDay(date) {
      return date.date.getMonth() === this.state.currentNumberMonth ? date.date.getDate() >= new Date().getDate() : false;
  }

  selectDay(day) {
      this.setState({activeDay: day})
      this.props.dispatch(actionCreators.selectDay(day));
      this.props.updateTimeLine(day, this.props.idCow);
  }

  _renderMonths() {
      return (
          <View style={styles.monthsScrollBar}>
              <View style={[styles.mothStyle, styles.divider]}><Text style={[styles.nameMonth, {fontFamily: "sf-ui-semibold"}]}>ИЮЛЬ</Text><View></View></View>

          </View>
      )
  }

  /*
  <View style={[styles.mothStyle]}><Text style={[styles.nameMonth, {color: "#222222"}]}>АВГУСТ</Text><View></View></View>
  <View style={[styles.mothStyle]}><Text style={[styles.nameMonth, {color: "#C2C2C2"}]}>СЕНТЯБРЬ</Text><View></View></View>
  */


  render() {
    const headerDaysOfWeek = data_calendar.daysOfWeek.map((day, ind) => {
        return (
            <View style={styles.dayStyle} key={ind}>
                <Text style={{color: "#333333", fontSize: 18, fontFamily: "sf-ui"}}>{day.toUpperCase()}</Text>
            </View>
        )
    });

    const datesOfMonth = this.weeks.map((week, indWeek) => {
        const datesOfWeek = week.map((date, indDate) => {
            return (
                <Day
                    key={indDate}
                    enabled={this._accessToDay.bind(this, date)()}
                    date={date.date}
                    selected={date.date.getDate() === this.state.activeDay.getDate()}
                    onPress={this.selectDay.bind(this, date.date)}
                />
            )
        })

        return (
            <View key={indWeek} style={styles.daysOfWeek}>
                {datesOfWeek}
            </View>
        )
    });

    var currentDate = `${this.state.activeDay.getDate()}.0${this.state.activeDay.getMonth() + 1}`

    return (
      <View style={styles.container}>
          {this._renderMonths.bind(this)()}
          <View style={styles.days}>

              <View>
                  <View style={styles.daysOfWeek}>
                      {headerDaysOfWeek}
                  </View>
                  {datesOfMonth}
              </View>
          </View>
          <Text style={styles.currentDate}>{currentDate}</Text>
      </View>
    );
  }
}


const mapStateToProps = (state) => ({
    schedule: state.schedule
});

export default connect(mapStateToProps)(Calendar)


const styles = StyleSheet.create({
  currentDate: {
    fontFamily: "sf-ui",
    fontSize: 12,
    lineHeight: 14,
    position: "absolute",
    backgroundColor: "#fff",
    bottom: -6,
    marginLeft: "auto",
    marginRight: "auto",
    paddingLeft: 20,
    paddingRight: 20
  },
  monthsScrollBar: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "center",
    marginTop: 7
  },
  monthStyle: {
    height: 20,
    justifyContent: "space-around",
    marginRight: 18
  },
  divider: {
    borderBottomColor: "#32CF86",
    borderBottomWidth: 2
  },
  container: {
    width: "100%",
    borderBottomWidth: 1,
    borderBottomColor: "#F1F1F1",
    marginBottom: 2,
    alignItems: "center",
    paddingTop: 40,
    paddingBottom: 50,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: "#fff"
  },
  headerText: {
    color: "#A2A2A2",
    fontSize: 16
  },
  nameMonth: {
    color: "#32CF86",
    fontSize: 18,
    fontFamily: "sf-ui",
    marginTop: 10,
    lineHeight: 31
  },
  days: {
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: 20,
  },
  daysOfWeek: {
    justifyContent: "space-around",
    flexDirection: "row",
    width: "100%",
    marginTop: 12
  },
  enabledDay: {
    color: "#A2A2A2",
  },
  disabledDay: {
    color: "rgba(200, 200, 200, .4)"
  },
  dayStyle: {
    flex: 1,
    alignItems: "center"
  }
});
