import React from 'react';
import AwesomeButton from './AwesomeButton.js';
import { ScrollView, StyleSheet, Image, TouchableOpacity, View, Text } from 'react-native';
import Svg, {
  Path, Rect
} from 'react-native-svg';


export default class MedDataListItem extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          isOpen: false
      }
  }
  static navigationOptions = {
      header: null
  }

  _renderDetails() {
      if (!this.props.status) {
          return (
              <View style={{width: "100%"}}><AwesomeButton text="Записаться на приём" /></View>
          )
      } else {
          return (
              <View>
                  <View style={styles.topDownloadActions}>
                      <View style={[styles.btnForm, {backgroundColor: "#FF7373"}]}>
                          <Text style={styles.pdfText}>PDF</Text>
                      </View>
                      <Text style={styles.descrText}>Иванов Ивано, {this.props.name}, 28.05.2019</Text>
                  </View>
                  <View style={{flexDirection: "row"}}>
                      <TouchableOpacity style={[styles.btnForm, {elevation: 2}]}>
                          <Svg width="8" height="20" viewBox="0 0 8 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <Path d="M3.64645 19.3536C3.84171 19.5488 4.15829 19.5488 4.35355 19.3536L7.53553 16.1716C7.7308 15.9763 7.7308 15.6597 7.53553 15.4645C7.34027 15.2692 7.02369 15.2692 6.82843 15.4645L4 18.2929L1.17157 15.4645C0.976311 15.2692 0.659728 15.2692 0.464466 15.4645C0.269204 15.6597 0.269204 15.9763 0.464466 16.1716L3.64645 19.3536ZM3.5 0L3.5 19H4.5L4.5 0L3.5 0Z" fill="#5C5C5C"/>
                          </Svg>
                      </TouchableOpacity>
                      <TouchableOpacity style={[styles.btnForm, {elevation: 2}]}>
                          <Svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <Rect x="0.5" y="0.5" width="21" height="15" stroke="#5C5C5C"/>
                          <Path d="M1 1L11 11L21 1" stroke="#5C5C5C"/>
                          </Svg>
                      </TouchableOpacity>
                  </View>
              </View>
          )
      }
  }

  render() {
      var rotateArrow = this.state.isOpen ? '180deg' : '0deg';
      var colorStatus = {
          backgroundColor: this.props.status ? this.props.result ? "#32CF86" : "#FF7373" : "#C4C4C4"
      };
      return (
        <TouchableOpacity style={styles.medDataListItem} onPress={() => {this.setState({isOpen: !this.state.isOpen})}}>
            <View style={styles.medDataListItemContent}>
                <Image style={{transform: [{rotate: rotateArrow}]}} source={require('../assets/icons/arrow_down.png')} />
                <Text style={[styles.mainText, {flex: 1, marginLeft: 20}]}>{this.props.name}</Text>
                <View style={[styles.indicator, colorStatus]}></View>
            </View>
            <View style={{display: this.state.isOpen ? "flex": "none"}}>{this._renderDetails.bind(this)()}</View>
        </TouchableOpacity>
      );
  }
}


const styles = StyleSheet.create({
  mainText: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: "sf-ui",
    color: "#5C5C5C"
  },
  indicator: {
    width: 25,
    height: 25,
    borderRadius: 25
  },
  medDataListItemContent: {
    flexDirection: "row",
    marginBottom: 25,
    justifyContent: "space-between",
    alignItems: "center"
  },
  medDataListItem: {
    marginBottom: 25,
    paddingRight: 40,
    paddingLeft: 40,
  },
  btnForm: {
    width: 62,
    height: 62,
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    marginRight: 11
  },
  topDownloadActions: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 11
  },
  pdfText: {
    fontFamily: "sf-ui",
    color: "#fff",
    fontSize: 14,
    lineHeight: 17
  },
  descrText: {
    fontFamily: "sf-ui",
    color: "#5C5C5C",
    fontSize: 10,
    lineHeight: 12
  }
});
