import React from 'react';
import { StyleSheet, TouchableOpacity, Text, Image } from 'react-native';
import { LinearGradient } from 'expo';

export default class AwesomeButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity style={styles.buttonStyle} onPress={ this.props.submitForm }>
          <Text style={styles.text}>{this.props.text}</Text>
          <Image style={styles.icon} source={this.props.source} />
      </TouchableOpacity>
    );
  }
}



const styles = StyleSheet.create({
  icon: {
    position: 'absolute',
    right: 30,
    top: 21
  },
  buttonStyle: {
    width: '100%',
    height: 56,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#32CF86",
    padding: 20,
    marginBottom: 20
  },
  text: {
    color: '#ffffff',
    fontSize: 18,
    fontFamily: "sf-ui"
  }
});
