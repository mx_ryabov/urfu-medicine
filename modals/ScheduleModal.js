import React, { Component } from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity
} from 'react-native';
import Modal from 'react-native-modal';



export default class ScheduleModal extends Component {



    render() {
        return (
            <Modal
                isVisible={this.props.isVisible}
                onBackdropPress={this.props.onClose}
                onBackButtonPress={this.props.onClose}
                animationIn={'slideInUp'}
                animationOut={'slideOutDown'}
                style={styles.modalStyle}
            >

            </Modal>
        );
    }
}

const styles = StyleSheet.create({
  
});
