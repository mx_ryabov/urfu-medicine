const config = require('./configure.js');
import { Alert } from 'react-native';


function postRequest(url, data) {
    console.log('post url: ', url, data);

    return fetch(config.urlServer + url, {
        method: "POST",
        headers: new Headers({ "Content-Type": "application/json" }),
        body: JSON.stringify(data)
    }).then((res) => {
        console.log('err::: ', res.status);
        if (res.status === 200) return res.json();
        Alert.alert("Ошибка сервера!");
    });
}

function getRequest(url, params) {
    let url_fetch = config.urlServer + url + "?";
    Object.keys(params).forEach(key => url_fetch += `${key}=${params[key]}&`);
    console.log('url::: ', url_fetch);

    return fetch(url_fetch, {
        method: "GET",
        headers: new Headers({ "Content-Type": "application/json" }),
    }).then((res) => {
        if (res.status === 200) return res.json();
        Alert.alert("Ошибка сервера!");
    });
}

function request(url, data, method) {

}

module.exports = {postRequest, getRequest};
