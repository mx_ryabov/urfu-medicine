Date.prototype.getNormalDay = function() {
    return this.getDay() === 0 ? 6 : this.getDay() - 1;
}

export let data_calendar = {
    months: [
      "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль",
      "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
    ],
    daysOfWeek: [
      "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"
    ],
    getWeeks: (numberYear, numberMonth) => {
      let dayFirstDay = new Date(numberYear, numberMonth).getNormalDay(); //день недели первого дня текущего месяц
      let lastDay = new Date(numberYear, numberMonth + 1, 0).getDate();


      let dayInc = 1 - dayFirstDay;
      let date = new Date(numberYear, numberMonth, dayInc);
      let month = [];

      do {
          let week = [];

          do {
              date = new Date(numberYear, numberMonth, dayInc);
              week.push({
                  date: date
              });
              dayInc++;
          } while (date.getNormalDay() < 6)

          month.push(week);
      } while (dayInc < lastDay || date.getNormalDay() !== 6)

      return month;
    }
}
