import { Alert } from 'react-native';
import {getRequest, postRequest} from "../helpers.js";
import {types} from '../reduxTypes.js';


// Helper functions to dispatch actions, optionally with payloads
export const actionCreators = {
  logout: () => {
      let req = postRequest( "/users/logout", {});
      return { type: types.LOGOUT, payload: req }
  },
  logoutSuccess: () => {
      return { type: types.LOGOUT_SUCCESS, payload: {} }
  },
  logoutError: (error) => {
      return { type: types.LOGOUT_ERROR, payload: error }
  },


  auth: (login, password) => {
      let req = postRequest( "/users/login", { login: login, password: password });
      return { type: types.AUTH, payload: req }
  },
  authSuccess: (me) => {
      return { type: types.AUTH_SUCCESS, payload: me }
  },
  authError: (error) => {
      return { type: types.AUTH_ERROR, payload: error }
  },


  selectDay: (day) => {
      return { type: types.SELECT_DATE, payload: day }
  },
  selectTimeLine: (time) => {
      return { type: types.SELECT_TIME_LINE, payload: time }
  },
}
