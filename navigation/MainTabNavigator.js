import React from 'react';
import { Platform, Image } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Svg, {
  Path
} from 'react-native-svg';

import TabBarIcon from '../components/TabBarIcon';
import SurveysScreen from '../screens/SurveysScreen';
import CertificatesScreen from '../screens/CertificatesScreen';
import NotificationsScreen from '../screens/NotificationsScreen';
import MedDataScreen from '../screens/MedDataScreen';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const SurveysStack = createStackNavigator(
  {
    SurveysCalendar: SurveysScreen,
  },
  config
);

SurveysStack.navigationOptions = {
  tabBarLabel: 'Обследования',
  tabBarIcon: ({ focused }) => {
    var source = focused ? require('../assets/icons/active_calendar.png') : require('../assets/icons/nonactive_calendar.png');
    return (<Image style={{width: 26, height: 26, resizeMode: 'contain'}} source={source} />)
  },
};

SurveysStack.path = '';

const CertificatesStack = createStackNavigator(
  {
    CertificatesList: CertificatesScreen,
  },
  config
);

CertificatesStack.navigationOptions = {
  tabBarLabel: 'Мои справки',
  tabBarIcon: ({ focused }) => {
    var source = focused ? require('../assets/icons/active_docs.png') : require('../assets/icons/nonactive_docs.png');
    return (<Image style={{width: 26, height: 26, resizeMode: 'contain'}} source={source} />)
  },
};

CertificatesStack.path = '';

const NotificationsStack = createStackNavigator(
  {
    NotificationsList: NotificationsScreen,
  },
  config
);

NotificationsStack.navigationOptions = {
  tabBarLabel: 'Уведомления',
  tabBarIcon: ({ focused }) => {
    var source = focused ? require('../assets/icons/active_notification.png') : require('../assets/icons/nonactive_notification.png');
    return (<Image style={{width: 26, height: 26, resizeMode: 'contain'}} source={source} />)
  },
};

NotificationsStack.path = '';

const MedDataStack = createStackNavigator(
  {
    MedDataProfile: MedDataScreen,
  },
  config
);

MedDataStack.navigationOptions = {
  tabBarLabel: 'Медданные',
  tabBarIcon: ({ focused }) => {
    var source = focused ? require('../assets/icons/active_meddata.png') : require('../assets/icons/nonactive_meddata.png');
    return (<Image style={{width: 26, height: 26, resizeMode: 'contain'}} source={source} />)
  },
};

MedDataStack.path = '';

const tabNavigator = createBottomTabNavigator({
  SurveysStack,
  CertificatesStack,
  NotificationsStack,
  MedDataStack
}, {
  tabBarOptions: {
    activeTintColor: '#32CF86',
    labelStyle: {
      fontSize: 9,
    },
    style: {
      height: 64,
      paddingTop: 5,
      paddingBottom: 8,
      elevation: 2
    }
  }
});

tabNavigator.path = '';

export default tabNavigator;
